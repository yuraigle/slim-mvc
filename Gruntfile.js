module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        uglify: {
            options: {
                mangle: false,
                compress: false
            },
            build: {
                src: [
                    'bower_components/jquery/dist/jquery.min.js',
                    'bower_components/bootstrap/dist/js/bootstrap.min.js',
                    'assets/scripts/main.js'
                ],
                dest: 'public/script.min.js'
            }
        },

        less: {
            dist: {
                options: {
                    cleancss: true,
                    compress: true,
                    modifyVars: {}
                },
                files: {
                    "public/style.min.css": "assets/styles/main.less"
                }
            }
        },

        copy: {
            main: {
                files: [
                    {expand: true, flatten: true, cwd: 'assets/images/', src: ['**'], dest: 'public/img/', filter: 'isFile'},
                    // include framework fonts and files
                    {expand: true, flatten: true, cwd: 'bower_components/bootstrap/dist/fonts', src: ['**'], dest: 'public/fonts', filter: 'isFile', dot: true},
                ]
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-less');

    grunt.registerTask('default', ['uglify', 'less', 'copy']);

};