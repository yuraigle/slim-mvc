<?php

require_once 'vendor/autoload.php';

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Console\ConsoleRunner;

$isDevMode = true;
$paths = array("App/Entity/");
$appConfig = include 'App/config/config.php';

$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
$config->setProxyDir('App/Entity/Proxy');
$config->setProxyNamespace('Proxy');

$entityManager = EntityManager::create($appConfig['doctrine'], $config);

return ConsoleRunner::createHelperSet($entityManager);
