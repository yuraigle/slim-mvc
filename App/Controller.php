<?php

namespace App;

use Slim\Slim;
use Doctrine\ORM\Configuration;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\ORM\EntityManager;

abstract class Controller
{
    /**
     * @var \Slim\Slim
     */
    protected $app;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    public function __construct(Slim $app = null)
    {
        $this->app = ($app instanceof Slim) ? $app : Slim::getInstance();
        $this->init();
    }

    public function init() {  }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function em()
    {
        if ($this->entityManager)
            return $this->entityManager;

        $config = new Configuration();
        $config->setMetadataCacheImpl(new ArrayCache());
        $driverImpl = $config->newDefaultAnnotationDriver(array(__DIR__ . '/Entity'));
        $config->setMetadataDriverImpl($driverImpl);
        $config->setProxyDir('App/Entity/Proxy');
        $config->setProxyNamespace('Proxy');

        $connectionOptions = include 'App/config/config.php';

        $this->entityManager = EntityManager::create($connectionOptions['doctrine'], $config);

        return $this->entityManager;
    }
}