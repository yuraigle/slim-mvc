<?php

namespace App;

class ViewHelper extends \Twig_Extension
{
    public function getName()
    {
        return 'view_helper';
    }

    public function getGlobals()
    {
        return ['session' => $_SESSION];
    }
}
