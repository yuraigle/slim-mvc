<?php

namespace App\Console;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Parse extends Command
{
    protected function configure()
    {
        $this->setName('app:parse')
             ->setDescription('Parse some content to our app');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("done");
    }
}
