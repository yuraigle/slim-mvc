<?php

namespace App\Console;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\Configuration;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\ORM\EntityManager;

abstract class Command extends \Symfony\Component\Console\Command\Command
{
    /**
     * @var \Slim\Slim
     */
    protected $app;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function em()
    {
        if ($this->entityManager)
            return $this->entityManager;

        $config = new Configuration();
        $config->setMetadataCacheImpl(new ArrayCache());
        $driverImpl = $config->newDefaultAnnotationDriver(array(__DIR__ . '/Entity'));
        $config->setMetadataDriverImpl($driverImpl);
        $config->setProxyDir(__DIR__ . '/Entity/Proxy');
        $config->setProxyNamespace('Proxy');

        $connectionOptions = include __DIR__ . '/../../App/config/config.php';

        $this->entityManager = EntityManager::create($connectionOptions['doctrine'], $config);

        return $this->entityManager;
    }

    // should be overridden
    protected function configure() {  }

    // should be overridden
    protected function execute(InputInterface $input, OutputInterface $output) {  }
}