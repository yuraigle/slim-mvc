<?php

namespace App\Controller;

use \App\Controller;

class IndexController extends Controller
{
    function init() {}

    public function indexAction()
    {
        $this->app->render('index/index.twig', []);
    }
}