<?php

namespace App\Controller;

use \App\Controller;

class AuthController extends Controller
{
    function init() {}

    public function loginAction()
    {
        $this->app->render('auth/login.twig', []);
    }

    public function loginPostAction()
    {
        $errors = [];
        $request = $this->app->request->params('user');

        // sanitize
        $email = trim($request['email']);
        $password = trim($request['password']);

        // validation
        $user = $this->em()->getRepository('\App\Entity\User')->findOneBy(['email' => $email]);
        if (!$user)
            $errors[] = "Wrong email or password";
        elseif (md5($password . $user->getSalt()) !== $user->getPassword())
            $errors[] = "Wrong email or password";

        if (empty($errors))
        {
            // save data in session
            $_SESSION['user'] = [
                "uid" => $user->getId(),
                "email" => $user->getEmail(),
            ];
            $this->app->flash('success', 'Successfully logged in');
            $this->app->redirect('/', 302);
        }
        else
        {
            $this->app->flashNow('error', join('|', $errors));
            $this->app->render('auth/login.twig', [
                "defaults" => $request
            ]);
        }
    }

    public function registerAction()
    {
        $this->app->render('auth/register.twig', []);
    }

    public function registerPostAction()
    {
        $errors = [];
        $request = $this->app->request->params('user');

        // sanitize
        $email = trim($request['email']);
        $password = trim($request['password']);

        // validation
        if (empty($email))
            $errors[] = "Email is empty";
        if (empty($password))
            $errors[] = "Password is empty";
        $cnt = $this->em()->createQuery('select count(u) from \App\Entity\User u where u.email = ?1')
            ->setParameter(1, $email)
            ->getSingleScalarResult();
        if ($cnt)
            $errors[] = "Email is already taken";

        if (empty($errors))
        {
            $user = new \App\Entity\User();
            $user->setEmail($email);
            $salt = uniqid(mt_rand(), true);
            $user->setSalt($salt);
            $user->setPassword(md5($password . $salt));
            $user->setRole('member');
            $this->em()->persist($user);
            $this->em()->flush();

            $_SESSION['user'] = [
                "uid" => $user->getId(),
                "email" => $user->getEmail(),
            ];
            $this->app->flash('success', 'Successfully registered');
            $this->app->redirect('/', 302);
        }
        else
        {
            $this->app->flashNow('error', join('|', $errors));
            $this->app->render('auth/register.twig', [
                "defaults" => $request
            ]);
        }
    }

    public function logoutAction()
    {
        $_SESSION['user'] = [];
        $this->app->flash('info', 'Successfully logged out');
        $this->app->redirect('/', 302);
    }

    public function forgotAction()
    {
        $this->app->render('auth/forgot.twig', []);
    }

    public function forgotPostAction()
    {
        $errors = [];
        $request = $this->app->request->params('user');

        // sanitize
        $email = trim($request['email']);

        // validation
        $user = $this->em()->getRepository('\App\Entity\User')->findOneBy(['email' => $email]);
        if (!$user)
            $errors[] = "Wrong email";

        if (empty($errors))
        {
            $code = substr(md5(uniqid(mt_rand(), true)), 0, 8);
            $user->setResetCode($code);
            $user->setResetAt(new \DateTime());
            $this->em()->persist($user);
            $this->em()->flush();

            // TODO: send email here

            $this->app->flash('info', 'We sent you a letter with instructions');
            $this->app->redirect('/', 302);
        }
        else
        {
            $this->app->flashNow('error', join('|', $errors));
            $this->app->render('auth/forgot.twig', [
                "defaults" => $request
            ]);
        }
    }

    public function resetAction($code)
    {
        $lo = new \DateTime();
        $lo->sub(new \DateInterval('PT1H'));
        $user = $this->em()->createQuery('select u from \App\Entity\User u where u.resetCode = ?1 and u.resetAt > ?2')
            ->setParameter(1, $code)
            ->setParameter(2, $lo)
            ->getResult();

        if (count($user) !== 1)
            throw new \Exception('Reset link is invalid');

        $this->app->render('auth/reset.twig', []);
    }

    public function resetPostAction($code)
    {
        $errors = [];
        $request = $this->app->request->params('user');

        $lo = new \DateTime();
        $lo->sub(new \DateInterval('PT1H'));
        $user = $this->em()->createQuery('select u from \App\Entity\User u where u.resetCode = ?1 and u.resetAt > ?2')
            ->setParameter(1, $code)
            ->setParameter(2, $lo)
            ->getResult();

        if (count($user) !== 1)
            throw new \Exception('Reset link is invalid');

        // sanitize
        $password = trim($request['password']);

        // validation
        if (empty($password))
            $errors[] = "Password is empty";

        if (empty($errors))
        {
            $salt = uniqid(mt_rand(), true);
            $user[0]->setSalt($salt);
            $user[0]->setPassword(md5($password . $salt));
            $this->em()->persist($user[0]);
            $this->em()->flush();

            $this->app->flash('info', 'New password saved');
            $this->app->redirect('/', 302);
        }
        else
        {
            $this->app->flashNow('error', join('|', $errors));
            $this->app->render('auth/reset.twig', []);
        }
    }
}