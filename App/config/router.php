<?php

// GET index route
$app->get('/', function() {
    $ctl = new \App\Controller\IndexController();
    $ctl->indexAction();
});

$app->notFound(function () use ($app) {
    $app->render('error/404.twig');
});

$app->error(function (\Exception $e) use ($app) {
    $app->render('error/500.twig', ['error' => $e]);
});

$app->get('/login', function() {
    $ctl = new \App\Controller\AuthController();
    $ctl->loginAction();
})->setName('login');

$app->post('/login', function() {
    $ctl = new \App\Controller\AuthController();
    $ctl->loginPostAction();
});

$app->get('/register', function() {
    $ctl = new \App\Controller\AuthController();
    $ctl->registerAction();
})->setName('register');

$app->post('/register', function() {
    $ctl = new \App\Controller\AuthController();
    $ctl->registerPostAction();
});

$app->get('/logout', function() {
    $ctl = new \App\Controller\AuthController();
    $ctl->logoutAction();
})->setName('logout');

$app->get('/forgot', function() {
    $ctl = new \App\Controller\AuthController();
    $ctl->forgotAction();
})->setName('forgot');

$app->post('/forgot', function() {
    $ctl = new \App\Controller\AuthController();
    $ctl->forgotPostAction();
});

$app->get('/reset/:code', function($code) {
    $ctl = new \App\Controller\AuthController();
    $ctl->resetAction($code);
})->setName('reset');

$app->post('/reset/:code', function($code) {
    $ctl = new \App\Controller\AuthController();
    $ctl->resetPostAction($code);
});
