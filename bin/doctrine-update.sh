#!/usr/bin/env bash
./vendor/bin/doctrine orm:clear-cache:metadata
./vendor/bin/doctrine orm:generate-entities .
./vendor/bin/doctrine orm:schema-tool:update --force
./vendor/bin/doctrine orm:generate-proxies
