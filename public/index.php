<?php

// Everything is relative to the application root now.
chdir(dirname(__DIR__));

require 'vendor/autoload.php';

$app = new \Slim\Slim(array(
    'debug' => true,
    'view' => new \Slim\Views\Twig(),
    'templates.path' => 'App/view/',
));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => 'tmp/cache',
);
$view->parserExtensions = array(
    new \Slim\Views\TwigExtension(),
    new \App\ViewHelper(),
);

$app->add(new \Slim\Middleware\SessionCookie(array(
    'expires' => '20 minutes',
    'path' => '/',
    'domain' => null,
    'secure' => false,
    'httponly' => false,
    'name' => 'slim_session',
    'secret' => 'GPL04LPikSve',
    'cipher' => MCRYPT_RIJNDAEL_256,
    'cipher_mode' => MCRYPT_MODE_CBC
)));

// load router files
require 'App/config/router.php';

$app->run();
