<?php

require_once 'vendor/autoload.php';

use Symfony\Component\Console\Application;

$application = new Application();
$application->add(new App\Console\Parse());
$application->run();
